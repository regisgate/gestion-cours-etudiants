 <!-- Header-->
        <header class="bg-dark py-5">
            <div class="container px-5">
                <div class="row gx-5 justify-content-center">
                    <div class="col-lg-6">
                        <div class="text-center my-5">
                            <h1 class="display-5 fw-bolder text-white mb-2">Bienvenu sur ProFormations</h1>
                            <p class="lead text-white-50 mb-4">Votre plateforme de receuil des tutoriels et formations sur mesure destinée aux générations actuelles et futures. Avec ProFormations, créer et diffuser vos contenus</p>
                            <div class="d-grid gap-3 d-sm-flex justify-content-sm-center">
                                <a class="btn btn-primary btn-lg px-4 me-sm-3" href="creerCompte">S'inscrire</a>
                                <a class="btn btn-success btn-lg px-4 me-sm-3" href="login">Se connecter</a>
                                <a class="btn btn-outline-light btn-lg px-4" href="cours">Catalogue des cours</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
