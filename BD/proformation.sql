-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 15 oct. 2021 à 13:43
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `proformation`
--

-- --------------------------------------------------------

--
-- Structure de la table `cours`
--

DROP TABLE IF EXISTS `cours`;
CREATE TABLE IF NOT EXISTS `cours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(25) NOT NULL,
  `titre` varchar(128) NOT NULL,
  `resume` text,
  `domaine` varchar(50) DEFAULT NULL,
  `langue` varchar(64) NOT NULL,
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cours`
--

INSERT INTO `cours` (`id`, `code`, `titre`, `resume`, `domaine`, `langue`, `create_at`) VALUES
(4, 'SVT001', 'SVT 3e', 'Cours de biologie pour les classes de 3e', 'Biologie', 'Fran&ccedil;ais', '2021-08-20 00:26:51'),
(8, 'TEST', 'Algo 1', 'Cours d\'initiation &agrave; l\'algo - niveau 1', 'Informatique', 'Fran&ccedil;ais', '2021-08-23 20:39:45'),
(9, 'IT001', 'Algo 1', 'Cours d\'initiation &agrave; l\'algorithme - Niveau 1', 'Informatique', 'Fran&ccedil;ais', '2021-08-24 12:36:10');

-- --------------------------------------------------------

--
-- Structure de la table `inscriptions`
--

DROP TABLE IF EXISTS `inscriptions`;
CREATE TABLE IF NOT EXISTS `inscriptions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `INE` varchar(25) NOT NULL,
  `code` varchar(25) NOT NULL,
  `inscriptionDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_cours` (`code`),
  KEY `fk_etudiant` (`INE`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `inscriptions`
--

INSERT INTO `inscriptions` (`id`, `INE`, `code`, `inscriptionDate`) VALUES
(2, '0123456', 'SVT001', '2021-08-24 02:25:10'),
(4, '0123456', 'TEST', '2021-08-24 12:34:48'),
(5, '2454N4', 'SVT001', '2021-08-28 08:47:53'),
(6, '2454N4', 'TEST', '2021-08-28 08:47:58'),
(8, '2454N4', 'IT001', '2021-09-07 14:47:53'),
(9, '5247fghn', 'SVT001', '2021-09-07 15:27:15'),
(10, '5247fghn', 'TEST', '2021-09-07 15:27:23');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
  `login` varchar(50) NOT NULL,
  `INE` varchar(25) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `ville` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `role` varchar(50) NOT NULL,
  `image` varchar(100) NOT NULL,
  `est_valide` tinyint(1) NOT NULL,
  `clef` int(11) NOT NULL,
  PRIMARY KEY (`login`),
  UNIQUE KEY `INE` (`INE`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`login`, `INE`, `nom`, `prenom`, `adresse`, `ville`, `password`, `mail`, `role`, `image`, `est_valide`, `clef`) VALUES
('admin', '01234', 'Admin', 'COMPTE', 'administration', '', '$2y$10$mJzoMCmaU1sgy4uOeoLl1OXWXcPvQbIDTIOmfcCd4mQumqDnGJ3b2', 'test@test.fr', 'administrateur', 'profils/test/1648_user-login-icon-20.jpg', 1, 0),
('bira', '0123456', 'BIRA', 'DJEKAMIAN', 'Quartier: Louis', 'Ouagadougou', '$2y$10$y3saTZqnC1j/avjcs4TQm.xesHQ.4V4zAa8S2W7V1fHH6qFC7Iv2m', 'biradjekamian@gmail.com', 'utilisateur', 'profils/profil.png', 1, 9343),
('gate', '2454N4', 'Maskemde', 'R&eacute;gis', '22 COURS FAURIEL', 'St Etienne', '$2y$10$KhDyOPSDD9QHBRJlyzfFd./X6vY1eo9pNORHo0HGQC7JzYI41mJlm', 'maskemderegis@gmail.com', 'administrateur', 'profils/gate/53327_regis.jpg', 1, 3558),
('marie', '5247fghn', 'Maskemde', 'Regis', '17 Boulevard Philippe Raoul Duval', 'Saint-&Eacute;tienne', '$2y$10$BcacNnDynDClZ0zNI0jp0esfZGPF/wqTorW6JjFppcopcS/5FSCdi', 'marie@gmail.com', 'utilisateur', 'profils/marie/11759_famille AZA.jpg', 1, 6676);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
